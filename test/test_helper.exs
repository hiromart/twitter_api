ExUnit.start

Mix.Task.run "ecto.create", ~w(-r TwitterApi.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r TwitterApi.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(TwitterApi.Repo)

