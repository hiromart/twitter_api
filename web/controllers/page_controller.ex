defmodule TwitterApi.PageController do
  use TwitterApi.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
